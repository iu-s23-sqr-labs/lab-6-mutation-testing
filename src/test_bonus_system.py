import math

from bonus_system import calculateBonuses


def test_bonus_system():
    test_cases = [
        (None, 0, 0),
        (None, 10000, 0.0),
        (None, 25000, 0.0),
        (None, 50000, 0),
        (None, 75000, 0),
        (None, 100000, 0.0),
        (None, 200000, 0.0),
        ('nothing', 0, 0),
        ('nothing', 10000, 0.0),
        ('nothing', 25000, 0.0),
        ('nothing', 50000, 0),
        ('nothing', 75000, 0),
        ('nothing', 100000, 0.0),
        ('nothing', 200000, 0.0),
        ('Standard', 0, 0.5),
        ('Standard', 10000, 0.75),
        ('Standard', 25000, 0.75),
        ('Standard', 50000, 1.0),
        ('Standard', 75000, 1.0),
        ('Standard', 100000, 1.25),
        ('Standard', 200000, 1.25),
        ('Premium', 0, 0.1),
        ('Premium', 10000, 0.15),
        ('Premium', 25000, 0.15),
        ('Premium', 50000, 0.2),
        ('Premium', 75000, 0.2),
        ('Premium', 100000, 0.25),
        ('Premium', 200000, 0.25),
        ('Diamond', 0, 0.2),
        ('Diamond', 10000, 0.3),
        ('Diamond', 25000, 0.3),
        ('Diamond', 50000, 0.4),
        ('Diamond', 75000, 0.4),
        ('Diamond', 100000, 0.5),
        ('Diamond', 200000, 0.5),
    ]

    for program, amount, expected in test_cases:
        result = calculateBonuses(program, amount)
        assert math.isclose(result, expected), f"Failed: {program}, {amount}, expected {expected}, got {result}"
